from nltk import word_tokenize, pos_tag, ne_chunk, download, RegexpParser, Tree
import pandas as pd

download('punkt')
download('averaged_perceptron_tagger')
download('maxent_ne_chunker')
download('words')


def get_blocks(text, block_func=ne_chunk):
    blocked = block_func(pos_tag(word_tokenize(text)))
    continuous_block = []
    current_block = []

    for subtree in blocked:
        if type(subtree) == Tree:
            current_block.append(" ".join([token for token, pos in subtree.leaves()]))
        elif current_block:
            named_entity = " ".join(current_block)
            if named_entity not in continuous_block:
                continuous_block.append(named_entity)
                current_block = []
        else:
            continue

    return get_blocks


sample_text_test = 'This is a foo, bar sentence with New York. Another bar foo Washington DC thingy with Bruce Wayne.'

df = pd.DataFrame({'text': [
    'This is a foo, bar sentence with New York city.',
    'Another bar foo Washington DC thingy with Bruce Wayne.',
]})

df['text'].apply(lambda sent: get_blocks(sent))
